import yaml
import math
import random
from pathlib import Path
import shapefile
import sys
from datetime import datetime
import uuid
import urllib.parse
import uuid
import shapefile
import shutil
import os
from PIL import Image
import base64
import logging
import hSatDBUtils


### HSat script Utils ###

"""
    load config from yaml file
    params:
        configFileName : the file path
"""
def loadConfig(configFilePath):
    fileConfig = None
    try:
        fileValid = Path(configFilePath)
        if (fileValid.exists()):
            with open(configFilePath, 'r') as f:
                fileConfig = yaml.load(f, Loader=yaml.FullLoader)
        else:
            logging.debug("file " + configFilePath + " not found")
    except Exception as e:
        logging.error("loadConfig,"+str(e))
    finally:
        return fileConfig

"""
    gets the value from shape record
    params : 
        shapeRecord : the shape record
        shapeAttr : the shape attributes
        attrValue : the attribute
        defaultReturn : the default value if None
"""
def getValue(shapeRecord, shapeAttributes, attrValue, defaultReturn):
    res = defaultReturn
    try:
        #extract the value 
        res = shapeRecord[shapeAttributes[attrValue]]
    except Exception as e:
        #TODO use logger library
        logging.warning('get value, '+ str(e) )
    finally:
        logging.debug(attrValue+" value = "+str(res))
        return res

"""
    extract the coordiantes of shape
"""
def getCoordinates(shapeGeo):
    res = []
    try:
        #gets the type of shape geo
        shpType = shapeGeo.shapeType
        if(shpType == shapefile.POLYGON):
            #polygon format : [[(...), .....]]
            logging.debug("polygon ")
            res = shapeGeo.__geo_interface__['coordinates'][0]
        elif (shpType == shapefile.POLYLINE):
            #polyLine format : [(...), .....]
            logging.debug("polyline ")
            res = shapeGeo.__geo_interface__['coordinates']
        elif (shpType == shapefile.POINT):
            logging.debug("point ")
            res = shapeGeo.__geo_interface__['coordinates']
    except Exception as e:
        logging.error("get coordinates,"+str(e))
    finally:
        return res
    
    """
        gets the shape position doc
        params:
            wgs84
            utm
    """
def getPositions(wgs84, utm=None, offsetCoordinates=None):
    res_positions = []
    # length positions
    # normal case length pg wgs84 & utm are same 
    # in order to avoid errors : use min() function
    try:
        positionsLen = len(wgs84)
        #gets coordinates mercator(utm)
        for i in range(positionsLen):
            mercator = {}
            gps={}
            wgsCoordinate = wgs84[i]
            if(utm):
                utmCoordinate = utm[i]

            offsetLat = 0
            offsetLon = 0
            if offsetCoordinates is not None:
                logging.debug("offset found")
                offsetLat = offsetCoordinates.get("lat")
                offsetLon = offsetCoordinates.get("lon")
            
            #wgs84 index 0 -> lon | index 1 -> lat
            gps["latitude"] = wgsCoordinate[1] + offsetLat
            gps["longitude"] = wgsCoordinate[0] + offsetLon
            
            #utm
            if(utm):
                mercator["x"] = utmCoordinate[0]
                mercator["y"] = utmCoordinate[1]
            
            #add the new position
            position={}
            position["gps"]=gps
            position["wgs84"]=gps
            position["mercator"]=mercator
            logging.debug(str(position))
            res_positions.append(position)
    except Exception as e:
        logging.error("get positions error ,"+str(e))
    finally:
        return res_positions
    
"""
    gets the asset position
"""
def getPosition(wgsCoordinate, utmCoordinate=None, offsetCoordinates=None):
    res_position={}
    try:
        mercator = {}
        gps={}

        offsetLat = 0
        offsetLon = 0
        if offsetCoordinates is not None:
            offsetLat = offsetCoordinates.get("lat")
            offsetLon = offsetCoordinates.get("lon")
            
        #wgs84 index 0 -> lon | index 1 -> lat
        gps["latitude"] = wgsCoordinate[1] + offsetLat
        gps["longitude"] = wgsCoordinate[0] + offsetLon
            
        #utm
        if(utmCoordinate):
            mercator["x"] = utmCoordinate[0]
            mercator["y"] = utmCoordinate[1]
            
        #add the new position
        res_position["gps"]=gps
        res_position["wgs84"]=gps
        res_position["mercator"]=mercator
        
    except Exception as e:
        logging.error("get position error ,"+str(e))
    finally:
        return res_position
    


"""
    generate new study(collect)
    return doc
"""
def generateStudy(project, collector, id_collect, shapeAttributes, shape_file_reader_wgs, pathImage, pathData, 
                        image_name, timestamp, datetime_str, shape_file_reader_utm=None):
    res_doc ={}
    try:
        res_doc["collect_name"] = datetime_str
        res_doc["collect_id"] = id_collect
        res_doc["collector_id"] = collector
        res_doc["origin"]="measure"
        res_doc["creation_date"]= timestamp
        res_doc["lastUpdate_date"]= timestamp

        #call generate image
        image = generateImage(image_name, pathImage, timestamp, pathData)

        project_data={}
        shapes=[]
        nb_shape = 1

        #browse records (shapes)
        for i in range(len(shape_file_reader_wgs)):
            #gets the shape informations
            shape_record = shape_file_reader_wgs.record(i)
            #gets the shape geometric data
            shape_geo_wgs = shape_file_reader_wgs.shape(i)
            shape_geo_utm =  None
            
            if(shape_file_reader_utm):
                shape_geo_utm = shape_file_reader_utm.shape(i) 
                
            #call generate shape
            shape = generateShape(shape_record, shape_geo_wgs, shapeAttributes, nb_shape, shape_geo_utm)
            shapes.append(shape)
            nb_shape +=1

        project_data["image"]=image
        project_data["shapes"]=shapes
        res_doc["project_data"]=project_data

    except Exception as e:
        logging.error('generate Study, '+str(e))
    finally:
        return res_doc

"""
    generate new study(collect) using only image file

"""
def generateStudyFromImg(project, collector, id_collect, pathImage, pathData, image_name, timestamp, datetime_str):
    res_doc ={}
    try:
        res_doc["collect_name"] = datetime_str
        res_doc["collect_id"] = id_collect
        res_doc["collector_id"] = collector
        res_doc["origin"]="measure"
        res_doc["creation_date"]= timestamp
        res_doc["lastUpdate_date"]= timestamp

        #call generate image
        image = generateImage(datetime_str, pathImage, timestamp, pathData)
        
        #empty
        shapes=[]
        project_data={}
        project_data["image"]=image
        project_data["shapes"]=shapes
        res_doc["project_data"]=project_data

    except Exception as e:
        logging.error("generate study from image "+ str(e))
    finally:
        return res_doc


"""
    gets the datetime value of study
"""
def getDatetimeStudy(shapeRecord, shapeAttributes):
    res_datetime = datetime.now()
    try:
        date_recorded = getValue(shapeRecord, shapeAttributes, 'date','')
        time_recorded = getValue(shapeRecord, shapeAttributes, 'time','')
        datetime_str = str(date_recorded)+' '+str(time_recorded)

        #convert into datetime
        # format : %Y%m%d %H%M%S
        res_datetime = datetime.strptime(datetime_str, '%Y%m%d %H%M%S')

        #replace year if defined (yaml file)
        yearAltered = shapeAttributes.get("yearAltered")
        if(yearAltered is not None):
            res_datetime = res_datetime.replace(year=yearAltered)
            logging.debug("year altered : "+str(yearAltered))

    except Exception as e:
        logging.error("get datetime study error, "+str(e))
    finally:
        logging.debug("date time value : "+str(res_datetime))
        return res_datetime

"""
    gets the datetime value of study (from image)
"""
def getDatetimeStudyFromImage(fileImage):
    res_datetime = datetime.now()
    try:
        nameImage = os.path.splitext(fileImage)[0]

        #image name 'expected' format : S1X_IW_%Y%m%d_%H%M%S_****
        nameImageList = nameImage.split('_')
        #index 2
        date_recorded = nameImageList[2] 
        #index 3
        time_recorded = nameImageList[3]

        datetime_str = str(date_recorded)+' '+str(time_recorded)
        #convert into datetime
        # format : %Y%m%d %H%M%S
        res_datetime = datetime.strptime(datetime_str, '%Y%m%d %H%M%S')

    except Exception as e:
        logging.error("get datetime study (image) error, "+str(e))
    finally:
        logging.debug("date time value : "+str(res_datetime))
        return res_datetime

"""
    add new site(project) into mongodb 
    if collection not found
    return doc
"""
def generateSite(project, collector_id):
    res_doc = {}
    try:
        # to change : gets the date from data
        timenow = datetime.now()
        #format datetime
        timenow_str = timenow.strftime('%d/%m/%Y %H:%M:%S')+" GMT"

        res_doc["display_name"] = project
        res_doc["creation_date"] = timenow_str
        res_doc["lastUpdate_date"] = timenow_str
        res_doc["creation_user"] = collector_id
        res_doc["project_id"] = project
        
        #collectors
        collectors=[]
        #default collector
        collector={}
        collector["collector_name"] = collector_id
        collector["collect_firstUpdate"] = timenow_str 
        collector["collect_lastUpdate"] = timenow_str
        collector["collector_id"] = collector_id
        collector["collect_user"] = collector_id
        collector["project_data"]= {}

        collectors.append(collector)
        res_doc["collectors"]=collectors

    except Exception as e:
        logging.error('generate Site, '+str(e))
    finally:
        logging.debug(str(res_doc))
        return res_doc

"""
    generate shape document
    params:
        shapefileRec : the shape file informations
        shapefileGeo : the shape file geographic data
"""
def generateShape(shapefileRec, shapefile_geo_wgs, shapeAttributes, nb_shape, shapefile_geo_utm=None):
    res_doc = {}

    try:
        id_shape=str(uuid.uuid4())
            
        res_doc["id"]=id_shape
        res_doc["name"]="PI "+str(nb_shape)
    
        #get coordinates
        wgsCoordinates = getCoordinates(shapefile_geo_wgs)
        utmCoordinates = None
        if(shapefile_geo_utm is not None):
            utmCoordinates = getCoordinates(shapefile_geo_utm)
            
        #get positions
        offsetCoordinates = shapeAttributes.get("offsetCoordinates")
        positions = getPositions(wgsCoordinates, utmCoordinates, offsetCoordinates)
        
        #get year : 
        yearAltered = shapeAttributes.get("yearAltered")
        #length proportion
        lenProportion = shapeAttributes.get("lengthProportion")
        
        res_doc["positions"]=positions
        #TODO length & surface values
        res_doc["length"] =  getValue(shapefileRec, shapeAttributes, "length", -1.0)
        res_doc["surface"] = getValue(shapefileRec, shapeAttributes, "surface", -1.0)
        res_doc["confidence_level"] = getValue(shapefileRec, shapeAttributes, "confidence", "")
        res_doc["asset"] = getValue(shapefileRec, shapeAttributes, "platform", "")

        #check asset value
        #convert into string if case
        if(type(res_doc["asset"]) is int):
            res_doc["asset"] = "P"+str(res_doc["asset"])

        if(res_doc["length"] == -1.0 and lenProportion is not None):
            #calculate the length using surface
            width = math.sqrt(res_doc["surface"]) / lenProportion
            res_doc["length"] = width*(lenProportion+1)*2
            logging.debug("new length : "+str(res_doc["length"]))

        if(yearAltered is not None):
            #generate random number between [-10%, 10%] ([-0.1, 0.1])
            randNum = random.uniform(-0.1, 0.1)
            logging.debug("rand : "+str(randNum))
            #update length and surface values
            res_doc["length"] += randNum * res_doc["length"]
            res_doc["surface"] += randNum * res_doc["surface"]

    except Exception as e:
        logging.error('generate shape error, '+str(e))
    finally:
        return res_doc

"""
    generates image doc
"""
def generateImage(image_name, pathImage, timestamp, pathData):
    res_doc = {}
    try:
        id_image = str(uuid.uuid4())
        res_doc["id"]=str(id_image)
        res_doc["date"]= timestamp
        res_doc["name_file"]=image_name
        other_informations={}
        
        #TODO review getsize(path)
        #other_informations["size"]=os.path.getsize(os.getcwd()+"/"+image_name+"_subset.tif")*1.0
        other_informations["size"]=os.path.getsize(pathImage)*1.0
        other_informations["fileSys_path"] = pathData
        res_doc["other_informations"] = other_informations
    except Exception as e:
        logging.error("generate image doc error"+str(e))
    finally:
        return res_doc

"""
    generates study document in order to save into ES
"""
def generateStudyforES(project_id, collector_id, collect_id, mongo_id_doc, timestamp):
    res = {}
    res["id_mongo"] = mongo_id_doc
    res["project_id"] = project_id
    res["collect_id"] = collect_id
    res["collector_id"] = collector_id
    res["dateMeasure"] = timestamp
    return res

"""
    generates study images
    params:
        image_name
        pathImage (path of the original image)
        pathData
        name_project
        collect_name
    TODO add image src (pathImage)
"""
def generateImages(image_name, pathImage, pathData, name_project, collect_name):
    res = False
    try:
        #path of current rep location (src images)
        path=os.getcwd()
        logging.debug(path)
        #image name file
        image_filename = os.path.basename(pathImage)
        logging.debug("img file name : "+image_filename)
        logging.debug(pathImage)
        #create jpeg & compressed images
        if(os.path.exists(pathImage)):
            tifFile = image_name+".tif"
            jpgfile = image_name+ ".jpg"
            jpgfile_compressed=image_name+"-compressed.jpg"
            im = Image.open(pathImage)
            imSize = im.size
            im.thumbnail(im.size)
            if(im.mode in ["RGBA", "P"]):
                im = im.convert('RGB')
            im.save(jpgfile, "JPEG", quality=100)
            #fix dimensions
            scaledSize = (int(imSize[0]*0.1), int(imSize[1]*0.1))
            imResized = im.resize(scaledSize, Image.ANTIALIAS)
            imResized.save(jpgfile_compressed,"JPEG",quality=15)
            logging.debug("images created")
            #create folder
            study_folder = os.path.join(path, name_project, collect_name)
            os.makedirs(study_folder)
            #copy image files into folder
            logging.debug("folder created")
            shutil.copy(pathImage, os.path.join(study_folder, tifFile))
            shutil.move(jpgfile, study_folder)
            shutil.move(jpgfile_compressed, study_folder)
            #move folder into dest
            path_dest = os.path.join(pathData, name_project, collect_name)
            logging.debug(path_dest)
            shutil.move(study_folder, path_dest)
            res= True
            logging.info("[+] Generating jpeg done!")
    except Exception as e:
            logging.error("[-] Error Generating jpeg!"+ str(e))
            raise e
    finally:
        return res


"""
generates the platforms
"""
def generateAssets(file, project_id, iconPath):
    try:
        res = []
        #TODO convert icon into base64
        iconBase64 = encodeFile(iconPath)
        logging.debug("icon val :"+str(iconBase64))
        nb_asset=1
        ct = datetime.now() 
        ts = ct.timestamp() *1000

        logging.debug("length file :"+str(len(file)));

        #browse records (assets)
        for i in range(len(file)):
            logging.debug("record  : "+str(i))
            asset ={}
            #gets the asset informations
            asset_record = file.record(i)
            #gets the shape geometric data
            asset_geo = file.shape(i)
            asset_name = "P"+str(nb_asset)
            id_asset=str(asset_name+"_"+str(uuid.uuid4()))

            #insert into dict
            asset["asset_id"] = id_asset
            asset["creation_date"] = ts
            asset["component_type"] = "asset"
            asset["component_name"] = asset_name
            asset["project_id"] =  project_id
            asset["icon"] = iconBase64
            coordinates = getCoordinates(asset_geo)
            position = getPosition(coordinates)
            asset["position"] = position

            #add asset
            res.append(asset)

            #next
            nb_asset += nb_asset

    except Exception as e:
        logging.error("generate assets error",+str(e))

    finally:
        return res


"""
encode file to base64 format
"""
def encodeFile(path):
    res = ""
    try:
        with open(path, "rb") as image_file:
            res = base64.b64encode(image_file.read())
    except Exception as e:
        logging.error("encodeImage error raised : "+str(e))
    finally:
        return res


"""
    generates a new notification after data insertion
"""
def generateNotification(idProject, name, idCollector="", message=""):
    notification={}
    try:
        dateNow = datetime.now()
        timestampNow = dateNow.timestamp()*1000

        notification['id_collector']=idCollector
        notification['id_project']=idProject
        notification['name']=name
        notification['message']=message
        notification['type']="Information"
        notification['level']="NORMAL"
        notification['time']=str(int(timestampNow))
        notification['occurrence_number']=str(1)

    except Exception as e:
        logging.error("add notification : "+str(e));
    finally:
        return notification

"""
    sends the notification to the admin
"""
def sendNotification(mongoClient, projectId):
    res= False
    try:
        # gets the recent id insered
        mongo_id_doc = hSatDBUtils.getLastIdDocMongo(mongoClient, "notifications")
        query = {"mail":"admin@halias.fr"}
        newValues = {"$push" : {"notifications_new": mongo_id_doc}}
        #update into superadmins collection
        res = hSatDBUtils.updateDocument(mongoClient, "superadmins", query, newValues)

        #for users
        query={"Projects.project_name" : projectId}
        #update into users collection
        res = hSatDBUtils.updateDocument(mongoClient, "users", query, newValues)


    except Exception as e:
        logging.error("send notification : "+str(e))

    finally:
        return res
    