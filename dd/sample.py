import sys
import os
import pymongo
from elasticsearch import Elasticsearch, helpers
import logging


"""
    connect to mongo db
    return mongoClient
"""
def connectMongo(mongoData):
    res = None
    try:
        logging.info(mongoData)
        MONGO_HOST = mongoData['host'] 
        MONGO_PORT = mongoData['port'] 
        MONGO_DB = mongoData['name'] 
        MONGO_USER = mongoData['user'] 
        MONGO_PASS = mongoData['pass'] 
        uri = "mongodb://{}:{}/{}?authSource=admin".format(MONGO_HOST, MONGO_PORT, MONGO_DB)
        #user & pass are defined
        if(len(MONGO_USER) > 0 ):
            uri = "mongodb://{}:{}@{}:{}/{}?authSource=admin".format(MONGO_USER, MONGO_PASS, MONGO_HOST, MONGO_PORT, MONGO_DB)
        client = pymongo.MongoClient(uri)
        client.server_info() # will throw an exception if failed
        res = client[MONGO_DB]
        logging.debug("[+] Connected to mongo db !")
    except Exception as e:
        logging.error('connect MongoDB, '+str(e))
    return res


"""
    connect to Elastic search
    return ES client
"""
def connectES(ESData):
    res = None
    try:
        logging.debug(ESData)
        ES_HOST = ESData['host']
        ES_PORT = ESData['port']
        es = Elasticsearch([ES_HOST],port=ES_PORT,timeout=30, retry_on_timeout=True)
        if not es.ping():
            raise ValueError('ES connection failed')
        res = es
        logging.info("[+] connected to ElasticSearch !")
    except Exception as e:
        logging.error('connect ElasticSearch, '+str(e))
    finally:
        return res
    
"""
    insert new document into mongodb
"""
def addDocumentToMongo(mongoClient, collection_name, doc):
    res = False
    try:
        site = mongoClient[collection_name]
        res = site.insert_one(doc)
        logging.debug("the insertion is done !")
    except Exception as e:
        logging.error("add document into mongo , "+str(e))
    return res

"""
    gets the last mongo id document
"""     
def getLastIdDocMongo(mongoClient, collection_name):
    res = None
    try:
        #TODO review gets the id of doc insered
        site = mongoClient[collection_name]
        document = site.find_one(
            {},
            sort=[('_id', pymongo.DESCENDING)]
            )
        res = str(document['_id'])
    except Exception as e:
        logging.error("get id mongo doc, "+str(e))
    return res

"""
    insert new document into elastic search
"""
def addDocumentToES(esClient, index, doc):
        res = False
        try:
            esClient.indices.create(index=index, ignore=400)
            res = esClient.index(index=index, body=doc)
            logging.debug("ES - add document is done")
        except Exception as e:
            logging.error('add document to ES error :'+ str(e))
        return res

"""
insert documents using bulk
"""
def addDocumentsES(esClient, index, docs):
    res = False
    try:
        #create if not exists
        esClient.indices.create(index=index, ignore=400)
        res = helpers.bulk(esClient, docs, index=index)
        logging.debug("ES - documents added")
    except Exception as e:
        logging.error("ES - add documents (bulk) error : "+str(e))
    return res

"""
    updates a document
"""
def updateDocument(mongoClient, collection_name, query, newValues):
    res = False
    try:
        site = mongoClient[collection_name]
        res = site.update_many(query, newValues)
        logging.debug("the update is done !")
    except Exception as e:
        logging.error("update document - mongo , "+str(e))
    return res
